/*
*********************************************************************************************************
*
*	模块名称 : 直流电机驱动（带编码器）
*	文件名称 : bsp_key.c
*	版    本 : V1.3
*	说    明 : 直流电机的正转，反转，停止都很简单。不过我在想怎么设计这个驱动。
*			   目前参考野火的代码，我想实现位置环，速度环，电流环的控制。
*
*	修改记录 :
*		版本号  日期       作者     	 说明
*		V1.0    2013-02-01 FlyyingPiggy  正式发布
*
*********************************************************************************************************
*/
#include "bsp.h"



typedef struct
{
	GPIO_TypeDef *IN1_GPIOx;
	uint16_t IN1_PIN;
	TIM_TypeDef *IN1_TIMx;
	uint8_t	IN1_CHx;
	GPIO_TypeDef *IN2_GPIOx;
	uint16_t IN2_PIN;
	TIM_TypeDef *IN2_TIMx;
	uint8_t	IN2_CHx;
}X_IN12_T;

/*定义直流电机的IN1 IN2*/
static const X_IN12_T s_dcmotor_list[DCM_COUNT] = {
	{GPIOA, GPIO_PIN_6, TIM3,1,GPIOA,GPIO_PIN_7,TIM3,2},		/* 直流电机1 */
};


/* 定义直流电机的状态 */
/* 如果要解耦这个结构体变量不应该给别的.C文件操作..这点我没做好。*/
DCMOROT_ATR DCMotor[DCM_COUNT];
/*
直流电机初始化
*/
void bsp_InitDcMotor(void)
{
	uint8_t _id;
	for(_id = 0; _id < DCM_COUNT; _id++)
	{
		DCMotor[_id].Dir = 0;
		DCMotor[_id].DownCount = 0;
		DCMotor[_id].UpCount = 0;
		DCMotor[_id].Position = 0;
		DCMotor[_id].Speed = 10000;/*占空比0~10000，万分之一精度*/
		DCMotor[_id].Count = 0;
		DCMotor[_id].State = 0;
		DCMotor[_id].PostionFlag = 0;
		DCMotor[_id].UpSetFlag = 0;
		DCMotor[_id].DownSetFlag = 0;
	}
}
/**
  * @brief  设置电机速度
* @param  _id:电机标号v: 速度（占空比）
  * @retval 无
  */
void bsp_set_DCMotor_speed(uint8_t _id,uint16_t v)
{
	if (_id >= DCM_COUNT)
	{
		/* 打印出错的源代码文件名、函数名称 */
		Error_Handler(__FILE__, __LINE__);
	}
	DCMotor[_id].Speed = v;
  	
}

/**
  * @brief  设置电机方向
  * @param  无
  * @retval 无
  */
void bsp_set_DCMotor_direction(uint8_t _id,uint8_t dir)
{
	if (_id >= DCM_COUNT)
	{
		/* 打印出错的源代码文件名、函数名称 */
		Error_Handler(__FILE__, __LINE__);
	}

	DCMotor[_id].Dir = dir;
}

/**
  * @brief  启用电机
* @param  _id:电机编号 _dir:电机方向 _distance:电机距离
  * @retval 无
  */
void bsp_set_DCMotor_enable(uint8_t _id)
{
	if (_id >= DCM_COUNT)
	{
		/* 打印出错的源代码文件名、函数名称 */
		Error_Handler(__FILE__, __LINE__);
	}
	DCMotor[_id].State = 1;
	if (DCMotor[_id].Dir == 0)//顺时针转
	{
		bsp_SetTIMOutPWM(s_dcmotor_list[_id].IN1_GPIOx,s_dcmotor_list[_id].IN1_PIN,s_dcmotor_list[_id].IN1_TIMx,s_dcmotor_list[_id].IN1_CHx,1000,DCMotor[_id].Speed);
		bsp_SetTIMOutPWM(s_dcmotor_list[_id].IN2_GPIOx,s_dcmotor_list[_id].IN2_PIN,s_dcmotor_list[_id].IN2_TIMx,s_dcmotor_list[_id].IN2_CHx,1000,0);
	}
	else//逆时针转
	{
		bsp_SetTIMOutPWM(s_dcmotor_list[_id].IN1_GPIOx,s_dcmotor_list[_id].IN1_PIN,s_dcmotor_list[_id].IN1_TIMx,s_dcmotor_list[_id].IN1_CHx,1000,0);
		bsp_SetTIMOutPWM(s_dcmotor_list[_id].IN2_GPIOx,s_dcmotor_list[_id].IN2_PIN,s_dcmotor_list[_id].IN2_TIMx,s_dcmotor_list[_id].IN2_CHx,1000,DCMotor[_id].Speed);
	}
	bsp_LedOn(0);
}

/**
  * @brief  禁用电机
  * @param  无
  * @retval 无
  */
void bsp_set_DCMotor_disable(uint8_t _id)
{
	if (_id >= DCM_COUNT)
	{
		/* 打印出错的源代码文件名、函数名称 */
		Error_Handler(__FILE__, __LINE__);
	}
	DCMotor[_id].State = 0;
	bsp_SetTIMOutPWM(s_dcmotor_list[_id].IN1_GPIOx,s_dcmotor_list[_id].IN1_PIN,s_dcmotor_list[_id].IN1_TIMx,s_dcmotor_list[_id].IN1_CHx,1000,10000);
	bsp_SetTIMOutPWM(s_dcmotor_list[_id].IN2_GPIOx,s_dcmotor_list[_id].IN2_PIN,s_dcmotor_list[_id].IN2_TIMx,s_dcmotor_list[_id].IN2_CHx,1000,10000);
	bsp_LedOff(0);
}


/**
  * @brief  电机位置式 PID 控制实现(定时调用)
  * @param  无
  * @retval 无
  */
void motor_pid_control(void)
{
//  if (DCMotor[0].State == 1)     // 电机在使能状态下才进行控制处理
//  {
//    float cont_val = 0;           // 当前控制值
//    int32_t Capture_Count = 0;    // 当前时刻总计数值
//    
//    /* 当前时刻总计数值 = 计数器值 + 计数溢出次数 * ENCODER_TIM_PERIOD  */
//    Capture_Count = __HAL_TIM_GET_COUNTER(&TIM_EncoderHandle) + (Encoder_Overflow_Count * ENCODER_TIM_PERIOD);
//    
//    cont_val = PID_realize(Capture_Count);    // 进行 PID 计算
//    
//    if (cont_val > 0)    // 判断电机方向
//    {
//      set_motor_direction(MOTOR_FWD);
//    }
//    else
//    {
//      cont_val = -cont_val;
//      set_motor_direction(MOTOR_REV);
//    }
//    
//    cont_val = (cont_val > PWM_MAX_PERIOD_COUNT*0.48) ? PWM_MAX_PERIOD_COUNT*0.48 : cont_val;    // 速度上限处理
//    set_motor_speed(cont_val);                                                         // 设置 PWM 占空比
//    
//  #if defined(PID_ASSISTANT_EN)
//    set_computer_value(SEND_FACT_CMD, CURVES_CH1, &Capture_Count, 1);                // 给通道 1 发送实际值
//  #else
//    printf("实际值：%d. 目标值：%.0f\n", actual_speed, get_pid_target());      // 打印实际值和目标值
//  #endif
//  }
}

