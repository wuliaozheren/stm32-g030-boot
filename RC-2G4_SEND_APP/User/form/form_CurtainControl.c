/*窗帘控制，提供上位机的API*/
#include "bsp.h"

/**/
static uint8_t g_Open  = 1;
static uint8_t g_Close = 0;
static uint8_t g_Dir   = 0;
/*参数初始化*/
void form_InitVar(void)
{
	if(g_Dir == 0x00)/*正方向*/
	{
		g_Open  = 0;
		g_Close = 1;		
	}
	else if(g_Dir == 0x01)/*反方向*/
	{
		g_Open  = 1;
		g_Close = 0;		
	}
}
/*打开窗帘*/
void form_CurtainOpen(void)
{
	bsp_set_DCMotor_direction(0,g_Open);
	bsp_set_DCMotor_enable(0);
	DCMotor[0].State = 0x01;
	bsp_StartAutoTimer(0, 500);
}
/*关闭窗帘*/
void form_CurtainClose(void)
{
	bsp_set_DCMotor_direction(0,g_Close);
	bsp_set_DCMotor_enable(0);
	DCMotor[0].State = 0x02;
	bsp_StartAutoTimer(0, 500);
}
/*
停止
直接停止会卡住，需要往反方向做一个回退。
*/
void form_Stop(void)
{
	//bsp_set_DCMotor_disable(0);/*先停止，防止小行程控制时候会有偏差*/
	DCMotor[0].Speed = 1500;
	if(DCMotor[0].State == 0x01)
	{
		bsp_set_DCMotor_direction(0,g_Close);
		bsp_set_DCMotor_enable(0);
	}
	else if(DCMotor[0].State == 0x02)
	{
		bsp_set_DCMotor_direction(0,g_Open);
		bsp_set_DCMotor_enable(0);
	}
	bsp_StopTimer(0);
	bsp_StartTimer(3,500);
}


/*运行百分比命令(0~100)*/
void form_CurtainLevel(uint8_t _Level)
{
	int16_t Position;
	int16_t PositionDel;
	//Level to Position
	Position =  DCMotor[0].UpCount + ((DCMotor[0].DownCount - DCMotor[0].UpCount) * _Level /100);
	DCMotor[0].Position = Position;
	if(DCMotor[0].Position >= DCMotor[0].Count)
	{
		PositionDel = DCMotor[0].Position - DCMotor[0].Count;
	}
	if(DCMotor[0].Position < DCMotor[0].Count)
	{
		PositionDel = DCMotor[0].Count - DCMotor[0].Position;
	}
	
	if(PositionDel > 50)
	{
		DCMotor[0].Speed = 10000;
	}
	else if(PositionDel <= 50 && PositionDel > 20)
	{
		DCMotor[0].Speed = PositionDel * 200;
	}
	else
	{
		DCMotor[0].Speed = 3000;
	}
	
	if(DCMotor[0].Count == DCMotor[0].Position)
	{
		//form_CurtainOpen();
	}
	else if(DCMotor[0].Count > DCMotor[0].Position)
	{
		DCMotor[0].PostionFlag = 1;
		bsp_StartAutoTimer(1,10);/*开启一个10ms定时器*/
		bsp_set_DCMotor_direction(0,g_Open);
		bsp_set_DCMotor_enable(0);
		DCMotor[0].State = 0x01;
		if(_Level == 0 || _Level == 100)
		{
			bsp_StartAutoTimer(0, 1000);
		}
	}
	else if(DCMotor[0].Count < DCMotor[0].Position)
	{
		DCMotor[0].PostionFlag = 1;
		bsp_StartAutoTimer(1,10);/*开启一个10ms定时器*/
		bsp_set_DCMotor_direction(0,g_Close);
		bsp_set_DCMotor_enable(0);
		DCMotor[0].State = 0x02;
		if(_Level == 0 || _Level == 100)
		{
			bsp_StartAutoTimer(0, 1000);
		}
	}
	
}




/*设置上行程*/
void form_SetUpstroke(uint8_t _State)
{
	if(_State == 0x01)
	{
		DCMotor[0].UpCount = DCMotor[0].Count;
		DCMotor[0].UpSetFlag = 1;
		DEBUG_LOG("设置上行程成功:%d\r\n",DCMotor[0].UpCount);
	}
	else if(_State == 0x00)
	{
		DEBUG_LOG("取消上行程\r\n");
	}
	if(DCMotor[0].DownSetFlag == 1 && DCMotor[0].UpSetFlag == 1)
	{
		g_CheckFlag = 1;
	}
}
/*设置下行程*/
void form_SetDownstroke(uint8_t _State)
{
	if(_State == 0x01)
	{
		DCMotor[0].DownCount = DCMotor[0].Count;
		DCMotor[0].DownSetFlag = 1;
		DEBUG_LOG("设置下行程成功:%d\r\n",DCMotor[0].DownCount);
	}
	else if(_State == 0x00)
	{
		DEBUG_LOG("取消下行程\r\n");
	}
	if(DCMotor[0].DownSetFlag == 1 && DCMotor[0].UpSetFlag == 1)
	{
		g_CheckFlag = 1;
	}
}
/*删除行程*/
void form_DeleteStroke(void)
{
	DCMotor[0].UpCount = 0;
	DCMotor[0].DownCount = 0;
}
/*读当前位置：0~0x64 ，0xff为找不到行程*/
uint8_t form_ReadPosition(void)
{
//Level to Position
//	Position =  DCMotor[0].UpCount + ((DCMotor[0].DownCount - DCMotor[0].UpCount) * _Level /100);
	uint8_t CurrentLevel;
	CurrentLevel = (float)(DCMotor[0].Count - DCMotor[0].UpCount)/(DCMotor[0].DownCount - DCMotor[0].UpCount) * 100;
	
	DEBUG_LOG("当前位置为:%d%%\r\n",CurrentLevel);
	return CurrentLevel;
}

/*读当前方向：正，反*/
uint8_t form_ReadDir(void)
{
	return g_Dir;
}

void form_WriteDir(uint8_t _Dir)
{
	g_Dir = _Dir;
	if(g_Dir == 0x00)/*正方向*/
	{
		g_Open  = 1;
		g_Close = 0;		
	}
	else if(g_Dir == 0x01)/*反方向*/
	{
		g_Open  = 0;
		g_Close = 1;		
	}
	else
	{
		return;
	}
}
/*读电机状态：停止，打开，关闭 , 堵转*/
uint8_t form_ReadState(void)
{
	uint8_t State;
	return State;
}
