/*
*********************************************************************************************************
*
*	模块名称 : ADC驱动
*	文件名称 : bsp_adc.c
*	版    本 : V1.0
*	说    明 : ADC_DMA采样
*
*	修改记录 :
*		版本号  日期        	作者     		说明
*		V1.0    2021-08-27  FlyyingPiggy  	正式发布。
*		V1.1	2021-09-13	FlyyingPiggy	弃用ADC采集方式判断遇阻。
*
*********************************************************************************************************
*/

#include "bsp.h"
uint16_t ADCxValues[4];
DMA_HandleTypeDef   DMA_Handle = {0};

/*
*********************************************************************************************************
*	函 数 名: bsp_InitADC
*	功能说明: 初始化ADC，采用DMA方式进行多通道采样，采集了PC0, Vbat/4, VrefInt和温度
*	形    参: 无
*	返 回 值: 无
*********************************************************************************************************
*/
void bsp_InitADC(void)
{
	ADC_HandleTypeDef   AdcHandle = {0};
	ADC_ChannelConfTypeDef   sConfig = {0};
	GPIO_InitTypeDef GPIO_InitStruct = {0};
	RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};
	
	/* ## - 1 - 配置ADC采样的时钟 ####################################### */
	PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_ADC;
	PeriphClkInit.Usart1ClockSelection = RCC_USART1CLKSOURCE_PCLK1;
	PeriphClkInit.AdcClockSelection = RCC_ADCCLKSOURCE_PLLADC;
	if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
	{
		Error_Handler(__FILE__,__LINE__);
	}
	
	/* ## - 2 - 配置ADC采样GPIO的时钟 ####################################### */
	__HAL_RCC_ADC_CLK_ENABLE();
    __HAL_RCC_GPIOB_CLK_ENABLE();
	__HAL_RCC_GPIOA_CLK_ENABLE();
    /**ADC1 GPIO Configuration
    PB0     ------> ADC1_IN8
    */
    GPIO_InitStruct.Pin = GPIO_PIN_0;
    GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
	
	GPIO_InitStruct.Pin = GPIO_PIN_11;
    GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
  
	/* ## - 3 - 配置ADC采样使用的时钟和DMA时钟优先级 ####################################### */
	__HAL_RCC_DMA1_CLK_ENABLE();
//	HAL_NVIC_SetPriority(DMA1_Channel1_IRQn, 0, 0);
//	HAL_NVIC_EnableIRQ(DMA1_Channel1_IRQn);
//	HAL_NVIC_SetPriority(DMA1_Ch4_5_DMAMUX1_OVR_IRQn, 0, 0);
//	HAL_NVIC_EnableIRQ(DMA1_Ch4_5_DMAMUX1_OVR_IRQn);
	
	DMA_Handle.Instance = DMA1_Channel1;				/* 使用的DMA1 Channel1 */
    DMA_Handle.Init.Request = DMA_REQUEST_ADC1;			/* 请求类型采用DMA_REQUEST_ADC3 */
    DMA_Handle.Init.Direction = DMA_PERIPH_TO_MEMORY;	/* 传输方向是从存储器到外设 */  
    DMA_Handle.Init.PeriphInc = DMA_PINC_DISABLE;		/* 外设地址自增禁止 */ 
    DMA_Handle.Init.MemInc = DMA_MINC_ENABLE;			/* 存储器地址自增使能 */  
    DMA_Handle.Init.PeriphDataAlignment = DMA_PDATAALIGN_HALFWORD; 	/* 外设数据传输位宽选择半字，即16bit */ 
    DMA_Handle.Init.MemDataAlignment = DMA_MDATAALIGN_HALFWORD;		/* 存储器数据传输位宽选择半字，即16bit */
    DMA_Handle.Init.Mode = DMA_CIRCULAR;/* 循环模式 */   
    DMA_Handle.Init.Priority = DMA_PRIORITY_VERY_HIGH;
	
	/* 初始化DMA */
	if(HAL_DMA_Init(&DMA_Handle) != HAL_OK)
	{
		Error_Handler(__FILE__, __LINE__);     
	}
    
	/* 关联ADC句柄和DMA句柄 */
	__HAL_LINKDMA(&AdcHandle, DMA_Handle, DMA_Handle);
	
    
	/* ## - 4 - 配置ADC ########################################################### */
	__HAL_RCC_ADC_CLK_ENABLE();
	
	AdcHandle.Instance = ADC1;
	AdcHandle.Init.ClockPrescaler = ADC_CLOCK_SYNC_PCLK_DIV4;
	AdcHandle.Init.Resolution = ADC_RESOLUTION_12B;
	AdcHandle.Init.DataAlign = ADC_DATAALIGN_RIGHT;
	AdcHandle.Init.ScanConvMode = ADC_SCAN_DISABLE;
	AdcHandle.Init.EOCSelection = ADC_EOC_SINGLE_CONV;
	AdcHandle.Init.LowPowerAutoWait = DISABLE;
	AdcHandle.Init.LowPowerAutoPowerOff = DISABLE;
	AdcHandle.Init.ContinuousConvMode = ENABLE;
	AdcHandle.Init.NbrOfConversion = 1;
	AdcHandle.Init.DiscontinuousConvMode = DISABLE;
	AdcHandle.Init.ExternalTrigConv = ADC_SOFTWARE_START;
	AdcHandle.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_RISING;
	AdcHandle.Init.DMAContinuousRequests = ENABLE;
	AdcHandle.Init.Overrun = ADC_OVR_DATA_PRESERVED;
	AdcHandle.Init.SamplingTimeCommon1 = ADC_SAMPLETIME_12CYCLES_5;
	AdcHandle.Init.SamplingTimeCommon2 = ADC_SAMPLETIME_12CYCLES_5;
	AdcHandle.Init.OversamplingMode = DISABLE;
	AdcHandle.Init.TriggerFrequencyMode = ADC_TRIGGER_FREQ_HIGH;
	
    /* 初始化ADC */
	if (HAL_ADC_Init(&AdcHandle) != HAL_OK)
	{
		Error_Handler(__FILE__, __LINE__);
	}
	
	/* 校准ADC */
	if (HAL_ADCEx_Calibration_Start(&AdcHandle) != HAL_OK)
	{
		Error_Handler(__FILE__, __LINE__);
	}	
  
	/* 配置ADC通道，序列1，采样PB0引脚 */
	/*
		采用PLL2时钟的话，ADCCLK = 72MHz / 8 = 9MHz
	    ADC采样速度，即转换时间 = 采样时间 + 逐次逼近时间
	                            = 810.5 + 8.5(16bit)
	                            = 820个ADC时钟周期
	    那么转换速度就是9MHz / 820 = 10975Hz
	*/
	sConfig.Channel = ADC_CHANNEL_8;
	sConfig.Rank = ADC_REGULAR_RANK_1;
	sConfig.SamplingTime = ADC_SAMPLINGTIME_COMMON_1;
	
	if (HAL_ADC_ConfigChannel(&AdcHandle, &sConfig) != HAL_OK)
	{
		Error_Handler(__FILE__, __LINE__);
	}
	
	sConfig.Channel = ADC_CHANNEL_9;
	sConfig.Rank = ADC_REGULAR_RANK_2;
	sConfig.SamplingTime = ADC_SAMPLINGTIME_COMMON_1;
	
	if (HAL_ADC_ConfigChannel(&AdcHandle, &sConfig) != HAL_OK)
	{
		Error_Handler(__FILE__, __LINE__);
	}
	/* ## - 6 - 启动ADC的DMA方式传输 ####################################### */
	if (HAL_ADC_Start_DMA(&AdcHandle, (uint32_t *)ADCxValues, 4) != HAL_OK)
	{
		Error_Handler(__FILE__, __LINE__);
	}
}



/*
*********************************************************************************************************
*	函 数 名: bsp_GetAdcValues
*	功能说明:  获取ADC的数据并打印(实际测试这个下面的计算公式，G030计算不过来，
			  我是10ms执行一次，结果卡死了。串口信息都收不到)
*	形    参: 无
*	返 回 值: 无
*********************************************************************************************************
*/
float bsp_GetAdcValues(void)
{
    float AdcValues;

    AdcValues = (ADCxValues[1] + ADCxValues[2] + ADCxValues[3] + ADCxValues[0] )/4 * 3.3 / 4096;
	
	return AdcValues;


}