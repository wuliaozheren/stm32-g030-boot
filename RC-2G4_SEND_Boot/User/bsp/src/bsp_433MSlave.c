/*
*********************************************************************************************************
*
*	模块名称 : 433M解码模块
*	文件名称 : bsp_Encoder.c
*	版    本 : V1.0
*	说    明 : 433M解码模块
*
*	修改记录 :
*		版本号  日期        		作者     		说明
*		V1.0    2021-09-07  	FlyyingPiggy  	正式发布
*
*********************************************************************************************************
*/
#include "bsp.h"

/**TIM16 GPIO Configuration
PB8     ------> TIM16_CH1
*/
#define TIM_IC 		TIM16
#define ICChannel	TIM_CHANNEL_1
#define ICGPIO		GPIOB
#define ICPIN		GPIO_PIN_8
#define ICAF		GPIO_AF2_TIM16


#define RCC_TIM_IC_CLK_ENABLE()		__HAL_RCC_TIM16_CLK_ENABLE()
#define RCC_GPIO_IC_CLK_ENABLE()	__HAL_RCC_GPIOB_CLK_ENABLE()
#define TIM_IC_IRQn					TIM16_IRQn
#define TIM_IC_IRQHandler			TIM16_IRQHandler


CAP_STATE g_Cap;/*结构体，用来保存捕获到的脉宽 高电平持续时间就代表脉宽，单位是us*/
TIM_HandleTypeDef   TimHandle = {0};
/*433M 数据解析采用输入捕获的方案*/

static uint8_t  _433State;/*433M解码的业务逻辑，状态机的状态*/
static uint32_t g_AddressCode[20];/*用于存放433M的地址，一共20个*/
static uint8_t g_AddCount;/*添码计数值*/

void bsp_Init433MSlave(uint32_t _ulFreq)
{
	TIM_IC_InitTypeDef sConfigIC = {0};
	GPIO_InitTypeDef GPIO_InitStruct = {0};
	uint8_t i;
	/* 参数初始化 */
	for(i = 0 ;i < 20 ; i++)
	{
		g_AddressCode[i] = FlashSaveData[i];
	}
	g_AddCount = FlashSaveData[20];
	
	
	/*计算定时器频率*/
	
	RCC_TIM_IC_CLK_ENABLE();
	TimHandle.Instance = TIM_IC;
	TimHandle.Init.Prescaler = 64-1;
	TimHandle.Init.CounterMode = TIM_COUNTERMODE_UP;
	TimHandle.Init.Period = 0xffff;
	TimHandle.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
	TimHandle.Init.RepetitionCounter = 0;
	TimHandle.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_ENABLE;
	if (HAL_TIM_Base_Init(&TimHandle) != HAL_OK)
	{
	Error_Handler(__FILE__,__LINE__);
	}
	
	/*使能定时器中断*/
	
	__HAL_TIM_ENABLE_IT(&TimHandle,TIM_IT_UPDATE);   //使能更新中断
	
	if (HAL_TIM_IC_Init(&TimHandle) != HAL_OK)
	{
	Error_Handler(__FILE__,__LINE__);
	}
	sConfigIC.ICPolarity = TIM_INPUTCHANNELPOLARITY_RISING;
	sConfigIC.ICSelection = TIM_ICSELECTION_DIRECTTI;
	sConfigIC.ICPrescaler = TIM_ICPSC_DIV1;
	sConfigIC.ICFilter = 0;
	if (HAL_TIM_IC_ConfigChannel(&TimHandle, &sConfigIC, ICChannel) != HAL_OK)
	{
	Error_Handler(__FILE__,__LINE__);
	}
	
	HAL_TIM_IC_Start_IT(&TimHandle,TIM_CHANNEL_1);   //开启TIM5的捕获通道1，并且开启捕获中断
    
	
	
	RCC_GPIO_IC_CLK_ENABLE();

	GPIO_InitStruct.Pin = ICPIN;
	GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	GPIO_InitStruct.Alternate = ICAF;
	HAL_GPIO_Init(ICGPIO, &GPIO_InitStruct);
	
	HAL_NVIC_SetPriority(TIM_IC_IRQn,2,0);    //设置中断优先级，抢占优先级2，子优先级0
    HAL_NVIC_EnableIRQ(TIM_IC_IRQn);          //开启ITM5中断通道  
}



/*用了STM32的输入捕获，用于捕获脉冲宽度*/
void TIM_IC_IRQHandler(void)
{
	uint16_t itstatus = 0x0;
	uint16_t itenable = 0x0;
	TIM_TypeDef* TIMx = TIM_IC;
	
	itstatus = TIMx->SR;
	itenable = TIMx->DIER & TIM_IT_CC1;
	
	if ((itstatus & TIM_IT_CC1) != (uint16_t)RESET)//捕获中断
	{
		 TIMx->SR = (uint16_t)~TIM_IT_CC1;//清除捕获中断
		 if(g_Cap.StartFlag == 0)/* 捕获到上升沿 */
		 {
			 g_Cap.StartFlag =1;
			 g_Cap.FinishFlag = 0;
			 g_Cap.RiseVal = 0;
			 g_Cap.OverVal = 0;
			 __HAL_TIM_DISABLE(&TimHandle);      		//关闭定时器5
			 __HAL_TIM_SET_COUNTER(&TimHandle,0);		//设置计数值
			 TIM_RESET_CAPTUREPOLARITY(&TimHandle,ICChannel);   //一定要先清除设置
			 TIM_SET_CAPTUREPOLARITY(&TimHandle,ICChannel,TIM_ICPOLARITY_FALLING);//设置为下降沿捕获
			 __HAL_TIM_ENABLE(&TimHandle);				//使能定时器
		 }
		 else
		 {
			 if( g_Cap.FinishFlag == 0)
			 {
				 g_Cap.FinishFlag = 1;
				 g_Cap.RiseVal = TIMx->CCR1;
				 TIM_RESET_CAPTUREPOLARITY(&TimHandle,ICChannel);   //一定要先清除设置
				 TIM_SET_CAPTUREPOLARITY(&TimHandle,ICChannel,TIM_ICPOLARITY_RISING);//设置为上升沿捕获
			 }

			 
		 }
	}
	
	if ((itstatus & TIM_IT_UPDATE) != (uint16_t)RESET)//更新中断
	{
		TIMx->SR = (uint16_t)~TIM_IT_UPDATE;//清除更新中断
		if(g_Cap.StartFlag == 1 && g_Cap.FinishFlag == 0)
		{
			g_Cap.OverVal++;
		}
		
	}
}




/*
*********************************************************************************************************
*	函 数 名: bsp_433Decode
*	功能说明: 433解码程序需要在时间片中轮询
*	形    参: _pAddressCode: 接收到的地址码存在这个地址
*			  _pCommandCode: 接收到的键值存放在这个地址
*	返 回 值: NULL
*********************************************************************************************************
*/
uint8_t bsp_433Decode(uint32_t *_pAddressCode,uint8_t *_pCommandCode)
{
	uint32_t width;
	static uint32_t AddressCode;
	static uint8_t  CommandCode;
	static uint8_t Count;
	static uint8_t State;
	static uint32_t  g_CheckAddressCode = 0x83A3C2C8;
	enum 
	{
		BOOT_CODE = 0, /* 检测引导码 */
		ADDRESS_CODE,  /* 检测地址码 */	  
		COMMAND_CODE,  /* 检测功能码 */
	}STATE_E;
	

	switch(State)
	{
		case BOOT_CODE:
			/* 代表捕获完成 */
			if(g_Cap.StartFlag == 1 && g_Cap.FinishFlag == 1)
			{	
				g_Cap.StartFlag = 0;
				Count = 0;
				AddressCode = 0;
				CommandCode = 0;
				width = g_Cap.OverVal * 0XFFFF + g_Cap.RiseVal;
				
				/* 脉宽在4600us~4900us之间，判断为引导码 */
				if(width < 4900 && width > 4600)
				{
					//printf("addresscode:");
					State = ADDRESS_CODE;
				}
			}
			break;
		case ADDRESS_CODE:
			if(g_Cap.StartFlag == 1 && g_Cap.FinishFlag == 1)
			{
				g_Cap.StartFlag = 0;
				width = g_Cap.OverVal * 0XFFFF + g_Cap.RiseVal;	
				/*读取32bit地址码*/
				if(Count==40)
				{
					*_pAddressCode = AddressCode;
					*_pCommandCode = CommandCode;
					State = BOOT_CODE;
					return 1;
				}
				else if(Count < 32)
				{
					/* 250us~ 400us 之间为0*/
					if(width < 500 && width > 250)
					{						
						AddressCode &= ~(1 << Count);
						Count++;
					}
					/* 600us~ 730us 之间为1*/
					else if(width < 730 && width > 600)
					{						
						AddressCode |= (1 << Count);
						Count++;
					}
					else
					{
						////DEBUG_LOG("width:%d\r\n",width);
						State = BOOT_CODE;
					}
					
				}
				/* 最后8bit为键值*/
				else if(Count >= 32)
				{
					if(width < 500 && width > 250)
					{						
						CommandCode &= ~(1 << (Count-32));
						Count++;
					}
					else if(width < 730 && width > 600)
					{						
						CommandCode |= (1 << (Count-32));
						Count++;
					}
					else
					{
						////DEBUG_LOG("width:%d\r\n",width);
						State = BOOT_CODE;			
					}					
				}
			}	
			break;	
	}
	return 0;
}

/* 下面是业务逻辑你不用看 我的业务是保存20个遥控器的地址码 */
static uint8_t MactchAddress(uint32_t _Address)
{
	uint8_t i;
	for(i = 0; i < 20 ;i ++)
	{
		if(g_AddressCode[i] == _Address)
		{
			return 1;
		}
	}
	return 0;
}

/* 把地址数据保存到FLASH */
void SaveAddress(void)
{
	uint8_t i;
	for(i = 0;i < 20 ;i++)
	{
		FlashSaveData[i] =  g_AddressCode[i];
	}
	FlashSaveData[20] = g_AddCount;
	
	STMFLASH_Write(FLASH_SAVE_ADDRESS,FlashSaveData,sizeof(FlashSaveData));	
}
/*433M转移到添码状态*/
void State2Add(void)
{
	_433State = 3;
}
/* 删除保存的地址码 */
void DelAddress(void)
{
	memset(g_AddressCode,0,sizeof(g_AddressCode));/*对码成功后，清除已添加的遥控器*/
	g_AddCount = 0;	
	_433State = 0;
	SaveAddress();
}
/* 433M的状态机大循环，在main函数中被轮询*/
void bsp_433Mpoll(void)
{
	uint32_t AddressCode;/* 433地址码 */
	uint8_t  CommandCode;/* 433M键值 */
	uint8_t  i;
	static uint8_t count;
	static uint8_t count_add;
	enum 
	{
		UN_ECODE = 0, 	/* 未对码状态 */
		ECODING,  		/* 对码状态 */	  
		ECODED,  		/* 完成对码状态 */
		ADD,			/* 添码*/
	}STATE_E;
	switch(_433State)
	{
		case UN_ECODE:	
			if(g_AddCount != 0 &&g_AddCount != 0xFF)
			{
				_433State = ECODED;
			}
			else
			{
				for(i = 0; i < 1; i++)
				{
					bsp_LedOn(0);
					bsp_DelayMS(500);
					bsp_LedOff(0);
					bsp_DelayMS(500);
				}
				//DEBUG_LOG("进入对码模式\r\n");
				_433State = ECODING;				
			}

			break;
		case ECODING:
			if(bsp_433Decode(&AddressCode,&CommandCode) != 0)
			{
				if(CommandCode == 0x33)
				{
					if(count == 1)
					{
						for(i = 0; i < 1; i++)
						{
							bsp_LedOn(0);
							bsp_DelayMS(500);
							bsp_LedOff(0);
							bsp_DelayMS(500);
						}
						count = 2;
					}
					else if(count == 0)
					{
						for(i = 0; i < 1; i++)
						{
							bsp_LedOn(0);
							bsp_DelayMS(500);
							bsp_LedOff(0);
							bsp_DelayMS(500);
						}
						count = 1;
					}
				}
				if((CommandCode == 0x88 || CommandCode == 0xCC) && count == 2)
				{
					count = 0;
					memset(g_AddressCode,0,sizeof(g_AddressCode));/*对码成功后，清除已添加的遥控器*/
					g_AddCount = 0;/* 对码成功会删除保存的地址 */
					g_AddressCode[g_AddCount] = AddressCode;
					g_AddCount++;
					SaveAddress();
					for(i = 0; i < 5; i++)
					{
						bsp_LedOn(0);
						bsp_DelayMS(500);
						bsp_LedOff(0);
						bsp_DelayMS(500);
					}
					_433State = ECODED;
				}
			}
			break;
		case ECODED:
			if(bsp_433Decode(&AddressCode,&CommandCode) != 0)
			{
				if(MactchAddress(AddressCode))
				{
					if(CommandCode == 0x88)
					{
						if(DCMotor[0].State == 0x01)//电机只接收 一次命令
						{
							break;
						}
						if(g_CheckFlag == 0)
						{
							DCMotor[0].Speed = 10000;
							form_CurtainOpen();
						}
						else
						{
							form_CurtainLevel(1);
						}
						//DEBUG_LOG("打开\r\n");
					}
					if(CommandCode == 0xAA)
					{
						form_Stop();
						//DEBUG_LOG("停止\r\n");
					}
					if(CommandCode == 0XCC)
					{
						if(DCMotor[0].State == 0x02)//电机只接收 一次命令
						{
							break;
						}
						if(g_CheckFlag == 0)
						{
							DCMotor[0].Speed = 10000;
							form_CurtainClose();
						}
						else
						{
							form_CurtainLevel(99);
						}						
						//DEBUG_LOG("关闭\r\n");
					}
					if(CommandCode == 0X33)
					{
						//DEBUG_LOG("设置\r\n");
					}				
				}

			}
			break;
		case ADD:
			if(bsp_433Decode(&AddressCode,&CommandCode) != 0)
			{
				if(CommandCode == 0x33)
				{
					if(count_add == 1)
					{
						for(i = 0; i < 1; i++)
						{
							bsp_LedOn(0);
							bsp_DelayMS(500);
							bsp_LedOff(0);
							bsp_DelayMS(500);
						}
						count_add = 2;
					}
					else if(count_add == 0)
					{
						for(i = 0; i < 1; i++)
						{
							bsp_LedOn(0);
							bsp_DelayMS(500);
							bsp_LedOff(0);
							bsp_DelayMS(500);
						}
						count_add = 1;
					}
				}
				if((CommandCode == 0x88 || CommandCode == 0xCC) && count_add == 2)
				{
					count_add = 0;
					if(MactchAddress(AddressCode))
					{
						
					}
					else
					{
						g_AddressCode[g_AddCount] = AddressCode;
						g_AddCount++;
						SaveAddress();
					}
					
					for(i = 0; i < 5; i++)
					{
						bsp_LedOn(0);
						bsp_DelayMS(500);
						bsp_LedOff(0);
						bsp_DelayMS(500);
					}
					_433State = ECODED;
				}
			}			
			break;
		default:
			break;
	}
}