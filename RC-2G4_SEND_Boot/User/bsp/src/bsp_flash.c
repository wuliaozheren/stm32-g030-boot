#include "bsp.h"

/*
address					    size	page	
0x0800F800 - 0x0800FFFF	 	2K		page31
*/

/* flash结束地址 */
#define USER_FLASH_END_ADDRESS        0x0800FFFF
/* 用户flash区大小 */
#define USER_FLASH_SIZE   (USER_FLASH_END_ADDRESS - APPLICATION_ADDRESS + 1)
/* 扇区0 - 3用于IAP BOOT, 下面是用户区首地址 */
#define APPLICATION_ADDRESS   (uint32_t)0x08003000 


uint32_t FlashSaveData[];
#define STM32_FLASH_BASE 0x08000000
#define FLASH_WAITETIME	10
uint32_t STMFLASH_ReadWord(uint32_t faddr)
{
	return *(volatile uint32_t*)faddr;
}

/*
*********************************************************************************************************
*	函 数 名: FLASH_If_Write
*	功能说明: 向flash中写数据，4字节对齐
*	形    参: WriteAddr	   flash起始地址
*             pBuffer      数据地址
*             NumToWrite   数据长度（4字节）
*	返 回 值: 0 成功
*             1 写flash过程中出错
*             2 写到flash中的数据跟读出的不相同
*********************************************************************************************************
*/
void STMFLASH_Write(uint32_t WriteAddr, uint32_t *pBuffer,uint32_t NumToWrite)
{
//	FLASH_EraseInitTypeDef FlashEraseInit;
//	HAL_StatusTypeDef FlashStatus = HAL_OK;
//	uint32_t PageError = 0;
//	uint32_t addrx = 0;
//	uint32_t endaddr = 0;
//	addrx = WriteAddr;
//	endaddr = WriteAddr + NumToWrite;
//	HAL_FLASH_Unlock();	
//	FlashStatus = FLASH_WaitForLastOperation(FLASH_WAITETIME);
//	if(FlashStatus == HAL_OK)
//	{
//		while(WriteAddr < endaddr)
//		{
//			if(HAL_FLASH_Program(FLASH_TYPEPROGRAM_DOUBLEWORD,WriteAddr,*(uint64_t*)pBuffer) != HAL_OK)
//			{
//				break;
//			}
//			if (*(uint32_t*)WriteAddr != *(uint32_t*)(pBuffer))
//			{
//				/* 读出的和写入的不相同 */
//				return (2);
//			}
//			WriteAddr += 8;
//			pBuffer += 2;
//		}
//	}
//	else
//	{
//		return (1);
//	}
//	HAL_FLASH_Lock();
//	
//	return (0);
}



/*
*********************************************************************************************************
*	函 数 名: FLASH_If_Erase
*	功能说明: 删除所有用户flash区
*	形    参: StartSector  起始扇区
*	返 回 值: 0 用户flash区成功删除
*             1 发送错误
*********************************************************************************************************
*/
uint32_t FLASH_If_Erase(void)
{
	FLASH_EraseInitTypeDef FlashEraseInit;
	uint32_t PageError = 0;

	HAL_FLASH_Unlock();
	FlashEraseInit.TypeErase = FLASH_TYPEERASE_PAGES;/*页编辑*/
	FlashEraseInit.Page = 7;/*从第几页开始*/
	FlashEraseInit.NbPages = 9;/*擦除几页*/
	if(HAL_FLASHEx_Erase(&FlashEraseInit,&PageError) != HAL_OK)
	{
		return (1);
	}
	FLASH_WaitForLastOperation(10);
	return (0);	
}
/*
*********************************************************************************************************
*	函 数 名: FLASH_If_Write
*	功能说明: 向flash中写数据，4字节对齐
*	形    参: FlashAddress flash起始地址
*             Data         数据地址
*             DataLength   数据长度（4字节）
*	返 回 值: 0 成功
*             1 写flash过程中出错
*             2 写到flash中的数据跟读出的不相同
*********************************************************************************************************
*/
uint32_t FLASH_If_Write(__IO uint32_t* FlashAddress, uint32_t* Data ,uint32_t DataLength)
{
	uint32_t i = 0;
	HAL_StatusTypeDef FlashStatus = HAL_OK;
	
	HAL_FLASH_Unlock();	
	FlashStatus = FLASH_WaitForLastOperation(FLASH_WAITETIME);
	if(FlashStatus == HAL_OK)
	{
		/**/
		for (i = 0; (i < DataLength) && (*FlashAddress <= (USER_FLASH_END_ADDRESS-4)); i=i+2)
		{
			/* Device voltage range supposed to be [2.7V to 3.6V], the operation will
			be done by word */ 
			if (HAL_FLASH_Program(FLASH_TYPEPROGRAM_DOUBLEWORD,*FlashAddress, *(uint64_t*)(Data+i)) == HAL_OK)
			{
				/* 检查写入的数据 */
				if (*(uint64_t*)*FlashAddress != *(uint64_t*)(Data+i))
				{
					/* 读出的和写入的不相同 */
					return(2);
				}
				
				/* 地址递增 */
				*FlashAddress += 8;
			}
			else
			{
				/* 向flash中写数据时发生错误 */
				return (1);
			}
		}		
	}
	HAL_FLASH_Lock();
	return (0);	
}
