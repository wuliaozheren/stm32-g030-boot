#include "bsp.h"
/*
该文件的优化等级单独设置为0，优化等级设置为3的时候，有一些代码会被优化掉。
其他的.C文件使用优化等级3以缩小bootloader程序的大小。
*/
#define YMODEMCOM COM1
uint8_t aFileName[FILE_NAME_LENGTH];
uint8_t aPacketData[PACKET_1K_SIZE + PACKET_DATA_INDEX + PACKET_TRAILER_SIZE];
HAL_StatusTypeDef Serial_PutByte( uint8_t param )
{
    comSendChar(YMODEMCOM,param);
    return 0;
}
/*
*********************************************************************************************************
*	函 数 名: UpdateCRC16
*	功能说明: 更新CRC16到input字节
*	形    参:  crc_in：端口号
*              byte：输入的字节
*	返 回 值:  无
*********************************************************************************************************
*/
uint16_t UpdateCRC16(uint16_t crc_in, uint8_t byte)
{
  uint32_t crc = crc_in;
  uint32_t in = byte | 0x100;

  do
  {
    crc <<= 1;
    in <<= 1;
    if(in & 0x100)
      ++crc;
    if(crc & 0x10000)
      crc ^= 0x1021;
  }
  
  while(!(in & 0x10000));

  return crc & 0xffffu;
}
/**
  * @brief  Convert a string to an integer
  * @param  p_inputstr: The string to be converted
  * @param  p_intnum: The integer value
  * @retval 1: Correct
  *         0: Error
  */
uint32_t Str2Int(uint8_t *p_inputstr, uint32_t *p_intnum)
{
  uint32_t i = 0, res = 0;
  uint32_t val = 0;

  if ((p_inputstr[0] == '0') && ((p_inputstr[1] == 'x') || (p_inputstr[1] == 'X')))
  {
    i = 2;
    while ( ( i < 11 ) && ( p_inputstr[i] != '\0' ) )
    {
      if (ISVALIDHEX(p_inputstr[i]))
      {
        val = (val << 4) + CONVERTHEX(p_inputstr[i]);
      }
      else
      {
        /* Return 0, Invalid input */
        res = 0;
        break;
      }
      i++;
    }

    /* valid result */
    if (p_inputstr[i] == '\0')
    {
      *p_intnum = val;
      res = 1;
    }
  }
  else /* max 10-digit decimal input */
  {
    while ( ( i < 11 ) && ( res != 1 ) )
    {
      if (p_inputstr[i] == '\0')
      {
        *p_intnum = val;
        /* return 1 */
        res = 1;
      }
      else if (((p_inputstr[i] == 'k') || (p_inputstr[i] == 'K')) && (i > 0))
      {
        val = val << 10;
        *p_intnum = val;
        res = 1;
      }
      else if (((p_inputstr[i] == 'm') || (p_inputstr[i] == 'M')) && (i > 0))
      {
        val = val << 20;
        *p_intnum = val;
        res = 1;
      }
      else if (ISVALIDDEC(p_inputstr[i]))
      {
        val = val * 10 + CONVERTDEC(p_inputstr[i]);
      }
      else
      {
        /* return 0, Invalid input */
        res = 0;
        break;
      }
      i++;
    }
  }

  return res;
}
/*
*********************************************************************************************************
*	函 数 名: Cal_CRC16
*	功能说明: 计算Ymodem协议的CRC校验和
*	形    参:  p_data:需要计算的数组
*              byte：数组的长度
*	返 回 值:  无
*********************************************************************************************************
*/
uint16_t Cal_CRC16(const uint8_t* p_data, uint32_t size)
{
  uint32_t crc = 0;
  const uint8_t* dataEnd = p_data+size;

  while(p_data < dataEnd)
    crc = UpdateCRC16(crc, *p_data++);
 
  crc = UpdateCRC16(crc, 0);
  crc = UpdateCRC16(crc, 0);

  return crc&0xffffu;
}

/**
  * @brief  Convert an Integer to a string
  * @param  p_str: The string output pointer
  * @param  intnum: The integer to be converted
  * @retval None
  */
/*
*********************************************************************************************************
*	函 数 名: Int2Str
*	功能说明: 整数转字符串
*	形    参:  p_str:字符串
*              intnum：整数
*	返 回 值:  无
*********************************************************************************************************
*/
void Int2Str(uint8_t *p_str, uint32_t intnum)
{
  uint32_t i, divider = 1000000000, pos = 0, status = 0;

  for (i = 0; i < 10; i++)
  {
    p_str[pos++] = (intnum / divider) + 48;

    intnum = intnum % divider;
    divider /= 10;
    if ((p_str[pos-1] == '0') & (status == 0))
    {
      pos = 0;
    }
    else
    {
      status++;
    }
  }
}

/*
*********************************************************************************************************
*	函 数 名: comGetBuf
*	功能说明: 循环从ReceiveBuf里读取size个数据，如果超过timeout没有读到数据就退出循环
*	形    参:  _ucPort：端口号
*              _pBbuf：数据
*              size：数据长度
*              timeout:超时时间
*	返 回 值:  详见HAL_StatusTypeDef
*********************************************************************************************************
*/
HAL_StatusTypeDef comGetBuf(COM_PORT_E _ucPort, uint8_t *_pBbuf, uint16_t size,uint16_t timeout)
{
    uint8_t pBufIndex = 0;
    bsp_StartTimer(0,timeout);/*启动软件定时器*/
    while(1)
    {

        if(pBufIndex < size)
        {
            if(comGetChar(_ucPort,&_pBbuf[pBufIndex]))/*读到指定长度的buf，退出循环*/
            {
                bsp_StartTimer(0,timeout);/*重置软件定时器*/
                pBufIndex++;/*数组下标增加*/
            }
        }
        else
        {
            return HAL_OK;
        }
       
        if(bsp_CheckTimer(0))/*定时器超时，退出循环*/
        {
            return HAL_TIMEOUT;
        }   
    }
}
/*
*********************************************************************************************************
*	函 数 名: ReceivePacket
*	功能说明: 接收一包数据
*	形    参: p_data：数据
*             p_length: 0代表结束 ，2 代表中止 , >0的其他数字代表packet长度
*             timeout: 超时时间
*   返 回 值:HAL_OK: 正常
*            HAL_BUSY: 被用户中止
*********************************************************************************************************
*/
static HAL_StatusTypeDef ReceivePacket(uint8_t *p_data, uint32_t *p_length, uint32_t timeout)
{
    uint32_t crc;
    uint32_t packet_size = 0;
    uint8_t status;
    uint8_t char1;

    *p_length = 0;
    status = comGetBuf(YMODEMCOM, &char1,1,timeout);

    if (status == HAL_OK)
    {
        switch (char1)
        {
            case SOH:
            packet_size = PACKET_SIZE;
            break;
            
            case STX:
            packet_size = PACKET_1K_SIZE;
            break;
            
            case EOT:
            break;
            
            case CA:
            if ((comGetBuf(YMODEMCOM, &char1, 1, timeout) == HAL_OK) && (char1 == CA))
            {
                packet_size = 2;
            }
            else
            {
                status = HAL_ERROR;
            }
            break;
            
            case ABORT1:
            case ABORT2:
            status = HAL_BUSY;
            break;
            
            default:
            status = HAL_ERROR;
            break;
        }
        *p_data = char1;

        if (packet_size >= PACKET_SIZE )
        {
            status = comGetBuf(YMODEMCOM, &p_data[PACKET_NUMBER_INDEX], packet_size + PACKET_OVERHEAD_SIZE, timeout);

            /* Simple packet sanity check */
            if (status == HAL_OK )
            {
                if (p_data[PACKET_NUMBER_INDEX] != ((p_data[PACKET_CNUMBER_INDEX]) ^ NEGATIVE_BYTE))
                {
                    packet_size = 0;
                    status = HAL_ERROR;
                }
                else
                {
                    /* Check packet CRC */
                    crc = p_data[ packet_size + PACKET_DATA_INDEX ] << 8;
                    crc += p_data[ packet_size + PACKET_DATA_INDEX + 1 ];
                    if (Cal_CRC16(&p_data[PACKET_DATA_INDEX], packet_size) != crc )
                    {
                        packet_size = 0;
                        status = HAL_ERROR;
                    }
                }
            }
            else
            {
                packet_size = 0;
            }
        }
    }
    *p_length = packet_size;
    return status;
}

/**
  * @brief  Prepare the first block
  * @param  p_data:  output buffer
  * @param  p_file_name: name of the file to be sent
  * @param  length: length of the file to be sent in bytes
  * @retval None
  */
static void PrepareIntialPacket(uint8_t *p_data, const uint8_t *p_file_name, uint32_t length)
{
  uint32_t i, j = 0;
  uint8_t astring[10];

  /* first 3 bytes are constant */
  p_data[PACKET_START_INDEX] = SOH;
  p_data[PACKET_NUMBER_INDEX] = 0x00;
  p_data[PACKET_CNUMBER_INDEX] = 0xff;

  /* Filename written */
  for (i = 0; (p_file_name[i] != '\0') && (i < FILE_NAME_LENGTH); i++)
  {
    p_data[i + PACKET_DATA_INDEX] = p_file_name[i];
  }

  p_data[i + PACKET_DATA_INDEX] = 0x00;

  /* file size written */
  Int2Str (astring, length);
  i = i + PACKET_DATA_INDEX + 1;
  while (astring[j] != '\0')
  {
    p_data[i++] = astring[j++];
  }

  /* padding with zeros */
  for (j = i; j < PACKET_SIZE + PACKET_DATA_INDEX; j++)
  {
    p_data[j] = 0;
  }
}

#define  APPLICATION_ADDRESS 0x80000000
COM_StatusTypeDef Ymodem_Receive ( uint32_t *p_size )
{
  uint32_t i, packet_length, session_done = 0, file_done, errors = 0, session_begin = 0;
  uint32_t flashdestination, ramsource, filesize;
  uint8_t *file_ptr;
  uint8_t file_size[FILE_SIZE_LENGTH], tmp, packets_received;
  COM_StatusTypeDef result = COM_OK;

  /* Initialize flashdestination variable */
  flashdestination = APPLICATION_ADDRESS;

  while ((session_done == 0) && (result == COM_OK))
  {
    packets_received = 0;
    file_done = 0;
    while ((file_done == 0) && (result == COM_OK))
    {
      switch (ReceivePacket(aPacketData, &packet_length, DOWNLOAD_TIMEOUT))
      {
        case HAL_OK:
          errors = 0;
          switch (packet_length)
          {
            case 2:
              /* Abort by sender */
              Serial_PutByte(ACK);
              result = COM_ABORT;
              break;
            case 0:
              /* End of transmission */
              Serial_PutByte(ACK);
              file_done = 1;
              break;
            default:
              /* Normal packet */
              if (aPacketData[PACKET_NUMBER_INDEX] != packets_received)
              {
                Serial_PutByte(NAK);
              }
              else
              {
                if (packets_received == 0)
                {
                  /* File name packet */
                  if (aPacketData[PACKET_DATA_INDEX] != 0)
                  {
                    /* File name extraction */
                    i = 0;
                    file_ptr = aPacketData + PACKET_DATA_INDEX;
                    while ( (*file_ptr != 0) && (i < FILE_NAME_LENGTH))
                    {
                      aFileName[i++] = *file_ptr++;
                    }

                    /* File size extraction */
                    aFileName[i++] = '\0';
                    i = 0;
                    file_ptr ++;
                    while ( (*file_ptr != ' ') && (i < FILE_SIZE_LENGTH))
                    {
                      file_size[i++] = *file_ptr++;
                    }
                    file_size[i++] = '\0';
                    Str2Int(file_size, &filesize);

                    /* Test the size of the image to be sent */
                    /* Image size is greater than Flash size */
                    if (*p_size > (USER_FLASH_SIZE + 1))
                    {
                      /* End session */
                      tmp = CA;
                      HAL_UART_Transmit(&UartHandle, &tmp, 1, NAK_TIMEOUT);
                      HAL_UART_Transmit(&UartHandle, &tmp, 1, NAK_TIMEOUT);
                      result = COM_LIMIT;
                    }
                    /* erase user application area */
                    FLASH_If_Erase(APPLICATION_ADDRESS);
                    *p_size = filesize;

                    Serial_PutByte(ACK);
                    Serial_PutByte(CRC16);
                  }
                  /* File header packet is empty, end session */
                  else
                  {
                    Serial_PutByte(ACK);
                    file_done = 1;
                    session_done = 1;
                    break;
                  }
                }
                else /* Data packet */
                {
                  ramsource = (uint32_t) & aPacketData[PACKET_DATA_INDEX];

                  /* Write received data in Flash */
                  if (FLASH_If_Write(flashdestination, (uint32_t*) ramsource, packet_length/4) == FLASHIF_OK)                   
                  {
                    flashdestination += packet_length;
                    Serial_PutByte(ACK);
                  }
                  else /* An error occurred while writing to Flash memory */
                  {
                    /* End session */
                    Serial_PutByte(CA);
                    Serial_PutByte(CA);
                    result = COM_DATA;
                  }
                }
                packets_received ++;
                session_begin = 1;
              }
              break;
          }
          break;
        case HAL_BUSY: /* Abort actually */
          Serial_PutByte(CA);
          Serial_PutByte(CA);
          result = COM_ABORT;
          break;
        default:
          if (session_begin > 0)
          {
            errors ++;
          }
          if (errors > MAX_ERRORS)
          {
            /* Abort communication */
            Serial_PutByte(CA);
            Serial_PutByte(CA);
          }
          else
          {
            Serial_PutByte(CRC16); /* Ask for a packet */
          }
          break;
      }
    }
  }
  return result;
}


///**
//  * @brief  Transmit a file using the ymodem protocol
//  * @param  p_buf: Address of the first byte
//  * @param  p_file_name: Name of the file sent
//  * @param  file_size: Size of the transmission
//  * @retval COM_StatusTypeDef result of the communication
//  */
//COM_StatusTypeDef Ymodem_Transmit (uint8_t *p_buf, const uint8_t *p_file_name, uint32_t file_size)
//{
//  uint32_t errors = 0, ack_recpt = 0, size = 0, pkt_size;
//  uint8_t *p_buf_int;
//  COM_StatusTypeDef result = COM_OK;
//  uint32_t blk_number = 1;
//  uint8_t a_rx_ctrl[2];
//  uint8_t i;
//#ifdef CRC16_F    
//  uint32_t temp_crc;
//#else /* CRC16_F */   
//  uint8_t temp_chksum;
//#endif /* CRC16_F */  

//  /* Prepare first block - header */
//  PrepareIntialPacket(aPacketData, p_file_name, file_size);

//  while (( !ack_recpt ) && ( result == COM_OK ))
//  {
//    /* Send Packet */
//    HAL_UART_Transmit(&UartHandle, &aPacketData[PACKET_START_INDEX], PACKET_SIZE + PACKET_HEADER_SIZE, NAK_TIMEOUT);

//    /* Send CRC or Check Sum based on CRC16_F */
//#ifdef CRC16_F    
//    temp_crc = Cal_CRC16(&aPacketData[PACKET_DATA_INDEX], PACKET_SIZE);
//    Serial_PutByte(temp_crc >> 8);
//    Serial_PutByte(temp_crc & 0xFF);
//#else /* CRC16_F */   
//    temp_chksum = CalcChecksum (&aPacketData[PACKET_DATA_INDEX], PACKET_SIZE);
//    Serial_PutByte(temp_chksum);
//#endif /* CRC16_F */

//    /* Wait for Ack and 'C' */
//    if (HAL_UART_Receive(&UartHandle, &a_rx_ctrl[0], 1, NAK_TIMEOUT) == HAL_OK)
//    {
//      if (a_rx_ctrl[0] == ACK)
//      {
//        ack_recpt = 1;
//      }
//      else if (a_rx_ctrl[0] == CA)
//      {
//        if ((HAL_UART_Receive(&UartHandle, &a_rx_ctrl[0], 1, NAK_TIMEOUT) == HAL_OK) && (a_rx_ctrl[0] == CA))
//        {
//          HAL_Delay( 2 );
//          __HAL_UART_FLUSH_DRREGISTER(&UartHandle);
//          result = COM_ABORT;
//        }
//      }
//    }
//    else
//    {
//      errors++;
//    }
//    if (errors >= MAX_ERRORS)
//    {
//      result = COM_ERROR;
//    }
//  }

//  p_buf_int = p_buf;
//  size = file_size;

//  /* Here 1024 bytes length is used to send the packets */
//  while ((size) && (result == COM_OK ))
//  {
//    /* Prepare next packet */
//    PreparePacket(p_buf_int, aPacketData, blk_number, size);
//    ack_recpt = 0;
//    a_rx_ctrl[0] = 0;
//    errors = 0;

//    /* Resend packet if NAK for few times else end of communication */
//    while (( !ack_recpt ) && ( result == COM_OK ))
//    {
//      /* Send next packet */
//      if (size >= PACKET_1K_SIZE)
//      {
//        pkt_size = PACKET_1K_SIZE;
//      }
//      else
//      {
//        pkt_size = PACKET_SIZE;
//      }

//      HAL_UART_Transmit(&UartHandle, &aPacketData[PACKET_START_INDEX], pkt_size + PACKET_HEADER_SIZE, NAK_TIMEOUT);
//      
//      /* Send CRC or Check Sum based on CRC16_F */
//#ifdef CRC16_F    
//      temp_crc = Cal_CRC16(&aPacketData[PACKET_DATA_INDEX], pkt_size);
//      Serial_PutByte(temp_crc >> 8);
//      Serial_PutByte(temp_crc & 0xFF);
//#else /* CRC16_F */   
//      temp_chksum = CalcChecksum (&aPacketData[PACKET_DATA_INDEX], pkt_size);
//      Serial_PutByte(temp_chksum);
//#endif /* CRC16_F */
//      
//      /* Wait for Ack */
//      if ((HAL_UART_Receive(&UartHandle, &a_rx_ctrl[0], 1, NAK_TIMEOUT) == HAL_OK) && (a_rx_ctrl[0] == ACK))
//      {
//        ack_recpt = 1;
//        if (size > pkt_size)
//        {
//          p_buf_int += pkt_size;
//          size -= pkt_size;
//          if (blk_number == (USER_FLASH_SIZE / PACKET_1K_SIZE))
//          {
//            result = COM_LIMIT; /* boundary error */
//          }
//          else
//          {
//            blk_number++;
//          }
//        }
//        else
//        {
//          p_buf_int += pkt_size;
//          size = 0;
//        }
//      }
//      else
//      {
//        errors++;
//      }

//      /* Resend packet if NAK  for a count of 10 else end of communication */
//      if (errors >= MAX_ERRORS)
//      {
//        result = COM_ERROR;
//      }
//    }
//  }

//  /* Sending End Of Transmission char */
//  ack_recpt = 0;
//  a_rx_ctrl[0] = 0x00;
//  errors = 0;
//  while (( !ack_recpt ) && ( result == COM_OK ))
//  {
//    Serial_PutByte(EOT);

//    /* Wait for Ack */
//    if (HAL_UART_Receive(&UartHandle, &a_rx_ctrl[0], 1, NAK_TIMEOUT) == HAL_OK)
//    {
//      if (a_rx_ctrl[0] == ACK)
//      {
//        ack_recpt = 1;
//      }
//      else if (a_rx_ctrl[0] == CA)
//      {
//        if ((HAL_UART_Receive(&UartHandle, &a_rx_ctrl[0], 1, NAK_TIMEOUT) == HAL_OK) && (a_rx_ctrl[0] == CA))
//        {
//          HAL_Delay( 2 );
//          __HAL_UART_FLUSH_DRREGISTER(&UartHandle);
//          result = COM_ABORT;
//        }
//      }
//    }
//    else
//    {
//      errors++;
//    }

//    if (errors >=  MAX_ERRORS)
//    {
//      result = COM_ERROR;
//    }
//  }

//  /* Empty packet sent - some terminal emulators need this to close session */
//  if ( result == COM_OK )
//  {
//    /* Preparing an empty packet */
//    aPacketData[PACKET_START_INDEX] = SOH;
//    aPacketData[PACKET_NUMBER_INDEX] = 0;
//    aPacketData[PACKET_CNUMBER_INDEX] = 0xFF;
//    for (i = PACKET_DATA_INDEX; i < (PACKET_SIZE + PACKET_DATA_INDEX); i++)
//    {
//      aPacketData [i] = 0x00;
//    }

//    /* Send Packet */
//    HAL_UART_Transmit(&UartHandle, &aPacketData[PACKET_START_INDEX], PACKET_SIZE + PACKET_HEADER_SIZE, NAK_TIMEOUT);

//    /* Send CRC or Check Sum based on CRC16_F */
//#ifdef CRC16_F    
//    temp_crc = Cal_CRC16(&aPacketData[PACKET_DATA_INDEX], PACKET_SIZE);
//    Serial_PutByte(temp_crc >> 8);
//    Serial_PutByte(temp_crc & 0xFF);
//#else /* CRC16_F */   
//    temp_chksum = CalcChecksum (&aPacketData[PACKET_DATA_INDEX], PACKET_SIZE);
//    Serial_PutByte(temp_chksum);
//#endif /* CRC16_F */

//    /* Wait for Ack and 'C' */
//    if (HAL_UART_Receive(&UartHandle, &a_rx_ctrl[0], 1, NAK_TIMEOUT) == HAL_OK)
//    {
//      if (a_rx_ctrl[0] == CA)
//      {
//          HAL_Delay( 2 );
//          __HAL_UART_FLUSH_DRREGISTER(&UartHandle);
//          result = COM_ABORT;
//      }
//    }
//  }

//  return result; /* file transmitted successfully */
//}