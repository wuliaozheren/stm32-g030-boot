#ifndef __BSP_FLASH_H
#define __BSP_FLASH_H
/* 一页2KB 存一页的数据 所以不要超过2048/4 */
extern uint32_t FlashSaveData[21];
void STMFLASH_Write(uint32_t WriteAddr, uint32_t *pBuffer,uint32_t NumToWrite);
void STMFLASH_Read(uint32_t ReadAddr, uint32_t *pBuffer, uint32_t NumToRead);
uint32_t FLASH_If_Write(__IO uint32_t* FlashAddress, uint32_t* Data ,uint32_t DataLength);
uint32_t FLASH_If_Erase(void);

#endif
