#ifndef __BSP_433MSLAVE_H
#define __BSP_433MSLAVE_H


typedef struct              
{   
	uint8_t   ucFinishFlag;   // 捕获结束标志位
	uint8_t   ucStartFlag;    // 捕获开始标志位
	uint16_t  usCtr;          // 捕获寄存器的值
	uint16_t  usPeriod;       // 自动重装载寄存器更新标志 
}STRUCT_CAPTURE;

typedef struct
{
	uint16_t RiseVal;		/* 高电平持续时间 */
	uint8_t  StartFlag;		/* 捕获开始标志 */
	uint8_t  FinishFlag;	/* 捕获结束标志 */
	uint16_t OverVal;		/* 溢出次数 */
}CAP_STATE;

extern CAP_STATE g_Cap;
void State2Add(void);
void DelAddress(void);
void bsp_Init433MSlave(uint32_t _ulFreq);
uint8_t bsp_433Decode(uint32_t *_pAddressCode,uint8_t *_pCommandCode);
void bsp_433Mpoll(void);
#endif
