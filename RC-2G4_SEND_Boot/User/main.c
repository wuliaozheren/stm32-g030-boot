#include "bsp.h"
/*
注意事项，串口的上位机需要采用GBK(GB2312)编码。

*/
/* 定义例程名和例程发布日期 */
#define EXAMPLE_NAME	"STM32G030_Boot"
#define EXAMPLE_DATE	"2022-01-01"
#define DEMO_VER		"1.0"

static void PrintfLogo(void);
static void PrintfHelp(void);
uint8_t readbuf[4] = {0x05,0x06,0x07,0x08};

int main(void)
{
	uint8_t read;
	bsp_Init();
	
//	PrintfLogo();
//	PrintfHelp();
//    bsp_StartAutoTimer(1, 100);	/* 启动1个100ms的自动重装的定时器 */
	Main_Menu();
    while(1)
	{	
		bsp_Idle();
        
        /* 判断定时器超时时间 */
		if (bsp_CheckTimer(1))	
		{
			/* 每隔100ms 进来一次 */  
			bsp_LedToggle(1);
		}
		
//		if(comGetChar(COM1,&read))
//		{
//			switch(read)
//			{
//				case 'C':
//					JumpToBootloader();
//					break;
//			}
//		}
		
	}
}



/*
*********************************************************************************************************
*	函 数 名: PrintfHelp
*	功能说明: 打印操作提示
*	形    参: 无
*	返 回 值: 无
*********************************************************************************************************
*/
static void PrintfHelp(void)
{
    printf("\n\r");
    printf("前12KB作为BOOT，后20KB作为APP使用\r\n");
	printf("输入C跳转到APP");
}

/*
*********************************************************************************************************
*	函 数 名: PrintfLogo
*	功能说明: 打印例程名称和例程发布日期, 接上串口线后，打开PC机的超级终端软件可以观察结果
*	形    参: 无
*	返 回 值: 无
*********************************************************************************************************
*/
static void PrintfLogo(void)
{
	printf("*************************************************************\n\r");	
	/* 检测CPU ID */
	{
		printf("\r\nCPU : STM32G030F6PF, SOP-20, 主频: %dMHz\r\n ,Flash:32KB ,RAM:8KB ", SystemCoreClock / 1000000);
	}

	printf("\n\r");
	printf("*************************************************************\n\r");
	printf("* 例程名称   : %s\r\n", EXAMPLE_NAME);	/* 打印例程名称 */
	printf("* 例程版本   : %s\r\n", DEMO_VER);		/* 打印例程版本 */
	printf("* 发布日期   : %s\r\n", EXAMPLE_DATE);	/* 打印例程日期 */
	printf("*************************************************************\n\r");
}
